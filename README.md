# Slides about Android Development

Repo of all of my slides related to Android content.


## Suggestions💡

You can suggest a new subject related to Android opening an issue for this repo.

---

## Content

#### Accessibility ♿️

- [More Accessible Apps](./accessibility/more_accessibile_apps_android.pdf) with Homer (The Simpsons)

- [Accessibility in Compose](./accessibility/accessibility_in_compose.pdf) with Rick and Morty

#### Code Patterns 🧑‍💻

- [Builder](./code_patterns/builder_pattern.pdf) with Commander Data (Star Trek - Next Generation)

- [Factory and Template Method](./code_patterns/factory_and_template_method%20_patterns.pdf) with Mr Neelix (Star Trek - Voyager)

- [List users - Android](./code_patterns/list_users_android%20.pdf)

- [Repository Pattern and Productivity](./code_patterns/repository_pattern_and_productivity.pdf)

- [Singleton and Prototype](./code_patterns/singleton_and_prototype_patterns.pdf) with Yoda (Star Wars)

- [Time Operations on Android](./code_patterns/time_operations_on_android.pdf) with Dr Emmett Brown and Marty Mcfly (Back to the future)

- [Working with Collections - Kotlin](./code_patterns/working_with_collections_kotlin.pdf)

- [Observable Data Holders](./code_patterns/observable_data_holders.pdf) with Dungeon Master (Dungeon and Dragons)

- [Flow - Under the water](./code_patterns/flow_under_the_water.pdf) with Spongebob, Patrick, and Squidward

#### Infra 📐

- [Bitrise - CI / CD for Android Projects](./infra/bitrise_cicd_for_android_projects.pdf) with Dr Nefario, Gru, and Minions

- [Encrypt vs Hashing](./infra/encrypt_vs_hashing.pdf) with Neo, Morpheus, and Trinity (Matrix)

- [Gradle Variables](./infra/gradle_variables.pdf) with Homer (The Simpsons)

- [Mockk and Truth](./infra/mockk_and_truth.pdf) with Spock (Star Trek - Enterprise)

- [My favorite tools](./infra/my_favorite_tools_moro.pdf)

- [DI world - Koin 3 Annotations](./infra/di_world_koin3_annotations.pdf) with Dr and Ensign Harry Kim (Star Trek - Voyager)

#### UI 🎨

- [Basic Layouts using Compose](./ui/basic_layouts_using_compose.pdf)

- [Constraint Layout in Compose](./ui/constraint_layout_in_compose.pdf) with Homer, and Lisa (The Simpsons)

- [Migrate an Existing App to Compose](./ui/migrate_an_existing_app_to_compose.pdf)

- [RecyclerView and Performance](./ui/recycler_view_and_performance.pdf)

- [State in Compose](./ui/state_in_compose.pdf) with Velma, Shaggy, and Scooby

---